/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int		key;
	char	name[20];
	int		grade;
}Data;

void insertionSort(Data data[], int n);
void insert(Data d, Data data[], int i);
void printArray(const Data data[],int size); 

int main()
{
	FILE*	inputFile = fopen("input.txt", "r");
	FILE*	outputFile;
	Data*	dataArray;
	int		size;
	int		i;

	/* input data */
	fscanf(inputFile, "%d", &size);
	dataArray = (Data*)malloc(sizeof(Data) * (size + 1));

	for (i = 1; i <= size; i++) {
		fscanf(inputFile, "%d %s %d ", 
			&dataArray[i].key, dataArray[i].name, &dataArray[i].grade);
	}

	fclose(inputFile);

	/* print input list */
	printf("<<<<<<<<<< Input List >>>>>>>>>> \n");
	printArray(dataArray, size);

	/* insertion sorting */
	insertionSort(dataArray, size);

	/* print sorted list */
	printf("<<<<<<<<<< Sorted List >>>>>>>>>> \n");
	printArray(dataArray, size);

	/* save result to file */
	outputFile = fopen("output.txt", "w");

	for (i = 1; i <= size; i++) {
		fprintf(outputFile, "%d %s %d\n", 
			dataArray[i].key, dataArray[i].name, dataArray[i].grade);
	}

	fclose(outputFile);

	return 0;
}

/**
 * sort a[1 : n] into nondecreasing order
 * @param	: Data array
 * @param	: Data array size
 */
void insertionSort(Data data[], int n)
{
	int j;

	for (j = 2; j <= n; j++) {
		Data temp = data[j];
		insert(temp, data, j - 1);
	}
}

/**
 * insert d into the ordered list data[1 : i] such that the 
 * resulting list a[1 : i + 1] is also ordered, the array data
 * must have space allocated for at least i + 2 elements
 */
void insert(Data d, Data data[], int i)
{
	data[0] = d;

	while (d.key < data[i].key) {
		data[i + 1] = data[i];
		i--;
	}
	data[i + 1] = d;
}

/**
 * print array data
 * @param	: data array
 * @param	: array size
 */
void printArray(const Data data[],int size) 
{
	int i;

	for (i = 1; i <= size; i++) {
		printf("< %d, %s, %d > \n", data[i].key, data[i].name, data[i].grade);
	}
	printf("\n");
}