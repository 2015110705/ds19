/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int value;
}Element;

int functionCalls;
int size;

void quickSort(Element arr[], int left, int right);
void printArray(Element arr[]);
void swap(Element* a, Element* b);

int main()
{
	FILE*		inputFile = fopen("input.txt", "r");
	FILE*		outputFile;
	Element*	elementArray;
	int			i;
	
	fscanf(inputFile, "%d ", &size);
	elementArray = (Element*)malloc(sizeof(Element) * size);

	for (i = 0; i < size; i++) {
		fscanf(inputFile, "%d ", &elementArray[i]);
	}

	fclose(inputFile);

	/* print list */
	printf("<<<<<<<<<< Input List >>>>>>>>>>\n");
	printArray(elementArray);

	/* quick sorting */
	printf("execution of quick sort ... \n");
	quickSort(elementArray, 0, size - 1);
	printf("calls of quick sort : %d \n\n", functionCalls);

	/* print list */
	printf("<<<<<<<<<< Sorted List >>>>>>>>>>\n");
	printArray(elementArray);

	/* save data */
	outputFile = fopen("output.txt", "w");

	fprintf(outputFile, "%d\n", size);
	for (i = 0; i < size; i++) {
		fprintf(outputFile, "%d ", elementArray[i]);
	}

	fclose(outputFile);

	return 0;
}

/**
 * sort arr[left : right] into nondecreasing order
 * on the key field; arr[left].value is arbitrarily
 * chosen as the pivot key; it is assumed that
 * arr[left].value <= arr[right + 1].value
 * @param	: Element array
 * @param	: left index
 * @param	: right index
 */
void quickSort(Element arr[], int left, int right)
{
	int		pivot, i, j;
	Element temp;

	functionCalls++;
	printArray(arr);
	
	if (left < right) {
		i = left;
		j = right + 1;
		pivot = arr[left].value;

		/**
		 * search for values from the left and right
		 * sublists, swapping out-of-order elements until
		 * the left and right boundaries cross or meet
		 */
		do {
			do i++; while (arr[i].value < pivot);
			do j--; while (arr[j].value > pivot);
			if (i < j) {
				swap(&arr[i], &arr[j]);
			}
		} while (i < j);
		swap(&arr[left], &arr[j]);

		//recursive call
		quickSort(arr, left, j - 1);
		quickSort(arr, j + 1, right);
	}
}

void printArray(Element arr[])
{
	int i;

	for (i = 0; i < size; i++) {
		printf("%2d ", arr[i].value);	
	}
	printf("\n");
}

void swap(Element* a, Element* b)
{
	Element temp = *a;
	*a = *b;
	*b = temp;
}